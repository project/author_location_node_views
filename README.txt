The Location module does not provide a way to display or filter node 
views, based on the location of their author user, provided by the User 
Locations submodule.

This module creates a views relationship betwen the nodes and the users, 
that you can use it after for getting fields or filters with the 
location of the authors of the nodes.

This is useful if you have for example a site with suppliers and product 
nodes added by those suppliers, products for wich you want to show the 
supplier location (a location for each product node beeing unuseful).
